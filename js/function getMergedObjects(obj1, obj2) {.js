function getMergedObjects(obj1, obj2) {
    const result = {};
    result.name = obj1.name;
    result.age = obj2.age;

    return result;

}

console.log(getMergedObjects({ name: 'Алиса' }, { age: 11 }))